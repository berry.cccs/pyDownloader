import logging
import unittest
import hashlib
from pyDownloader import pyDownloader
from pyDownloader.protocols.sftp import SFTPDownloader
from pyDownloader.protocols.exceptions import ProtocolNotConfiguredException
logging.basicConfig(level=logging.DEBUG)

# ======================================================================================================================
# Steps to get this tests running (Docker installation required and current user can create/remove images)
# ======================================================================================================================
# 1) Open a terminal in the tests/dockerfiles folder
# 2) Build the docker container for sftp `docker build -f sftp/dockerfile -t pydownloader/sftp .`
# 3) Run the docker container for sftp `docker run -p 22:22 -p 23:22 -ti pydownloader/sftp /bin/bash`
# 4) Start the SSH Daemon in foreground `/sbin/sshd -Dd`
# 5) In Pycharm, run the unit tests for sftp
# 6) ???
# 7) Profit
# 8) Once you are done tinkering with the SFTP protocol, you can break the execution of the daemon `ctrl + c` and exit
# the container using exit
# 9 [ optional ]) If you want to remove the built image from docker: `docker -f rmi pydownloader/sftp`
# ======================================================================================================================
#TODO: Implement docker build + run in python


class TestSFTPDownloader(unittest.TestCase):
    _logger = logging.getLogger(__name__)

    # Using port 22
    # Using password authentication
    # Using absolute paths
    downloader_simple_passwd_root_p22 = SFTPDownloader('sftp://testuser:testpassword@example.com:/simplepath',
                                                       '/tmp/unittest')

    downloader_complex_passwd_root_p22 = SFTPDownloader('sftp://testuser:testpassword@example.com:/complex/path',
                                                        '/tmp/unittest')
    # Using relative paths
    downloader_simple_passwd_rel_p22 = SFTPDownloader('sftp://testuser:testpassword@example.com:simplepath',
                                                      '/tmp/unittest')

    downloader_complex_passwd_rel_p22 = SFTPDownloader('sftp://testuser:testpassword@example.com:complex/path',
                                                       '/tmp/unittest')
    # Using private key authentication
    # Using absolute paths
    downloader_simple_key_root_p22 = SFTPDownloader('sftp://testuser@example.com:/simplepath',
                                                    '/tmp/unittest',
                                                    private_key='../dockerfiles/sftp/sshkey')

    downloader_complex_key_root_p22 = SFTPDownloader('sftp://testuser@example.com:/complex/path',
                                                     '/tmp/unittest',
                                                     private_key='../dockerfiles/sftp/sshkey')
    # Using relative paths
    downloader_simple_key_rel_p22 = SFTPDownloader('sftp://testuser@example.com:simplepath',
                                                   '/tmp/unittest',
                                                   private_key='../dockerfiles/sftp/sshkey')

    downloader_complex_key_rel_p22 = SFTPDownloader('sftp://testuser@example.com:complex/path',
                                                    '/tmp/unittest',
                                                    private_key='../dockerfiles/sftp/sshkey')

    # Using Port 23
    # Using password authentication
    # Using absolute paths
    downloader_simple_passwd_root_p23 = SFTPDownloader('sftp://testuser:testpassword@example.com:/simplepath',
                                                       '/tmp/unittest',
                                                       port=23)

    downloader_complex_passwd_root_p23 = SFTPDownloader('sftp://testuser:testpassword@example.com:/complex/path',
                                                        '/tmp/unittest',
                                                        port=23)
    # Using relative paths
    downloader_simple_passwd_rel_p23 = SFTPDownloader('sftp://testuser:testpassword@example.com:simplepath',
                                                      '/tmp/unittest',
                                                      port=23)

    downloader_complex_passwd_rel_p23 = SFTPDownloader('sftp://testuser:testpassword@example.com:complex/path',
                                                       '/tmp/unittest',
                                                       port=23)
    # Using private key authentication
    # Using absolute paths
    downloader_simple_key_root_p23 = SFTPDownloader('sftp://testuser@example.com:/simplepath',
                                                    '/tmp/unittest',
                                                    private_key='../dockerfiles/sftp/sshkey',
                                                    port=23)

    downloader_complex_key_root_p23 = SFTPDownloader('sftp://testuser@example.com:/complex/path',
                                                     '/tmp/unittest',
                                                     private_key='../dockerfiles/sftp/sshkey',
                                                     port=23)
    # Using relative paths
    downloader_simple_key_rel_p23 = SFTPDownloader('sftp://testuser@example.com:simplepath',
                                                   '/tmp/unittest',
                                                   private_key='../dockerfiles/sftp/sshkey',
                                                   port=23)

    downloader_complex_key_rel_p23 = SFTPDownloader('sftp://testuser@example.com:complex/path',
                                                    '/tmp/unittest',
                                                    private_key='../dockerfiles/sftp/sshkey',
                                                    port=23)

    def test_initialization(self):
        self._logger.debug('Port 22, password auth, simple absolute path')
        self.assertEqual('testuser', self.downloader_simple_passwd_root_p22.username)
        self.assertEqual('testpassword', self.downloader_simple_passwd_root_p22.password)
        self.assertEqual('example.com', self.downloader_simple_passwd_root_p22.host)
        self.assertEqual('/simplepath', self.downloader_simple_passwd_root_p22.path)
        self.assertEqual(None, self.downloader_simple_passwd_root_p22.private_key)

        self._logger.debug('Port 22, password auth, complex absolute path')
        self.assertEqual('testuser', self.downloader_complex_passwd_root_p22.username)
        self.assertEqual('testpassword', self.downloader_complex_passwd_root_p22.password)
        self.assertEqual('example.com', self.downloader_complex_passwd_root_p22.host)
        self.assertEqual('/complex/path', self.downloader_complex_passwd_root_p22.path)
        self.assertEqual(None, self.downloader_complex_passwd_root_p22.private_key)

        self._logger.debug('Port 22, password auth, simple relative path')
        self.assertEqual('testuser', self.downloader_simple_passwd_rel_p22.username)
        self.assertEqual('testpassword', self.downloader_simple_passwd_rel_p22.password)
        self.assertEqual('example.com', self.downloader_simple_passwd_rel_p22.host)
        self.assertEqual('simplepath', self.downloader_simple_passwd_rel_p22.path)
        self.assertEqual(None, self.downloader_simple_passwd_rel_p22.private_key)

        self._logger.debug('Port 22, password auth, complex relative path')
        self.assertEqual('testuser', self.downloader_complex_passwd_rel_p22.username)
        self.assertEqual('testpassword', self.downloader_complex_passwd_rel_p22.password)
        self.assertEqual('example.com', self.downloader_complex_passwd_rel_p22.host)
        self.assertEqual('complex/path', self.downloader_complex_passwd_rel_p22.path)
        self.assertEqual(None, self.downloader_complex_passwd_rel_p22.private_key)

        self._logger.debug('Port 22, private key auth, simple absolute path')
        self.assertEqual('testuser', self.downloader_simple_key_root_p22.username)
        self.assertEqual(None, self.downloader_simple_key_root_p22.password)
        self.assertEqual('example.com', self.downloader_simple_key_root_p22.host)
        self.assertEqual('/simplepath', self.downloader_simple_key_root_p22.path)
        self.assertEqual('../dockerfiles/sftp/sshkey',
                         self.downloader_simple_key_root_p22.private_key)

        self._logger.debug('Port 22, private key auth, complex absolute path')
        self.assertEqual('testuser', self.downloader_complex_key_root_p22.username)
        self.assertEqual(None, self.downloader_complex_key_root_p22.password)
        self.assertEqual('example.com', self.downloader_complex_key_root_p22.host)
        self.assertEqual('/complex/path', self.downloader_complex_key_root_p22.path)
        self.assertEqual('../dockerfiles/sftp/sshkey',
                         self.downloader_complex_key_root_p22.private_key)

        self._logger.debug('Port 22, private key auth, simple relative path')
        self.assertEqual('testuser', self.downloader_simple_key_rel_p22.username)
        self.assertEqual(None, self.downloader_simple_key_rel_p22.password)
        self.assertEqual('example.com', self.downloader_simple_key_rel_p22.host)
        self.assertEqual('simplepath', self.downloader_simple_key_rel_p22.path)
        self.assertEqual('../dockerfiles/sftp/sshkey',
                         self.downloader_simple_key_rel_p22.private_key)

        self._logger.debug('Port 22, private key auth, complex relative path')
        self.assertEqual('testuser', self.downloader_complex_key_rel_p22.username)
        self.assertEqual(None, self.downloader_complex_key_rel_p22.password)
        self.assertEqual('example.com', self.downloader_complex_key_rel_p22.host)
        self.assertEqual('complex/path', self.downloader_complex_key_rel_p22.path)
        self.assertEqual('../dockerfiles/sftp/sshkey',
                         self.downloader_complex_key_rel_p22.private_key)

        self._logger.debug('Port 23, password auth, simple absolute path')
        self.assertEqual('testuser', self.downloader_simple_passwd_root_p23.username)
        self.assertEqual('testpassword', self.downloader_simple_passwd_root_p23.password)
        self.assertEqual('example.com', self.downloader_simple_passwd_root_p23.host)
        self.assertEqual('/simplepath', self.downloader_simple_passwd_root_p23.path)
        self.assertEqual(None, self.downloader_simple_passwd_root_p23.private_key)

        self._logger.debug('Port 23, password auth, complex absolute path')
        self.assertEqual('testuser', self.downloader_complex_passwd_root_p23.username)
        self.assertEqual('testpassword', self.downloader_complex_passwd_root_p23.password)
        self.assertEqual('example.com', self.downloader_complex_passwd_root_p23.host)
        self.assertEqual('/complex/path', self.downloader_complex_passwd_root_p23.path)
        self.assertEqual(None, self.downloader_complex_passwd_root_p23.private_key)

        self._logger.debug('Port 23, password auth, simple relative path')
        self.assertEqual('testuser', self.downloader_simple_passwd_rel_p23.username)
        self.assertEqual('testpassword', self.downloader_simple_passwd_rel_p23.password)
        self.assertEqual('example.com', self.downloader_simple_passwd_rel_p23.host)
        self.assertEqual('simplepath', self.downloader_simple_passwd_rel_p23.path)
        self.assertEqual(None, self.downloader_simple_passwd_rel_p23.private_key)

        self._logger.debug('Port 23, password auth, complex relative path')
        self.assertEqual('testuser', self.downloader_complex_passwd_rel_p23.username)
        self.assertEqual('testpassword', self.downloader_complex_passwd_rel_p23.password)
        self.assertEqual('example.com', self.downloader_complex_passwd_rel_p23.host)
        self.assertEqual('complex/path', self.downloader_complex_passwd_rel_p23.path)
        self.assertEqual(None, self.downloader_complex_passwd_rel_p23.private_key)

        self._logger.debug('Port 23, private key auth, simple absolute path')
        self.assertEqual('testuser', self.downloader_simple_key_root_p23.username)
        self.assertEqual(None, self.downloader_simple_key_root_p23.password)
        self.assertEqual('example.com', self.downloader_simple_key_root_p23.host)
        self.assertEqual('/simplepath', self.downloader_simple_key_root_p23.path)
        self.assertEqual('../dockerfiles/sftp/sshkey',
                         self.downloader_simple_key_root_p23.private_key)

        self._logger.debug('Port 23, private key auth, complex absolute path')
        self.assertEqual('testuser', self.downloader_complex_key_root_p23.username)
        self.assertEqual(None, self.downloader_complex_key_root_p23.password)
        self.assertEqual('example.com', self.downloader_complex_key_root_p23.host)
        self.assertEqual('/complex/path', self.downloader_complex_key_root_p23.path)
        self.assertEqual('../dockerfiles/sftp/sshkey',
                         self.downloader_complex_key_root_p23.private_key)

        self._logger.debug('Port 23, private key auth, simple relative path')
        self.assertEqual('testuser', self.downloader_simple_key_rel_p23.username)
        self.assertEqual(None, self.downloader_simple_key_rel_p23.password)
        self.assertEqual('example.com', self.downloader_simple_key_rel_p23.host)
        self.assertEqual('simplepath', self.downloader_simple_key_rel_p23.path)
        self.assertEqual('../dockerfiles/sftp/sshkey',
                         self.downloader_simple_key_rel_p23.private_key)

        self._logger.debug('Port 23, private key auth, complex relative path')
        self.assertEqual('testuser', self.downloader_complex_key_rel_p23.username)
        self.assertEqual(None, self.downloader_complex_key_rel_p23.password)
        self.assertEqual('example.com', self.downloader_complex_key_rel_p23.host)
        self.assertEqual('complex/path', self.downloader_complex_key_rel_p23.path)
        self.assertEqual('../dockerfiles/sftp/sshkey',
                         self.downloader_complex_key_rel_p23.private_key)

        with self.assertRaises(ProtocolNotConfiguredException):
            SFTPDownloader('sftp://testuser@example.com/simplepath', '/tmp/unittest')

        with self.assertRaises(ProtocolNotConfiguredException):
            SFTPDownloader('sftp://testuser:example.com/complex/path', '/tmp/unittest')

    def test_download_password_relative_p22(self):
        self._logger.debug('Download with password and relative path on port 22')
        comparison_hash = '9cce32b2829b5c73b70075cef7210458'
        local_file = '/tmp/file1'
        remote_file = 'sftp://root:password@test.local:testfile'

        downloader = SFTPDownloader(remote_file, local_file)

        downloader.download()

        while not downloader.completed():
            pass

        downloaded_md5 = hashlib.md5()
        with open(local_file, 'rb') as f:
            for chunk in iter(lambda: f.read(8192), b""):
                downloaded_md5.update(chunk)

        self.assertEqual(comparison_hash, downloaded_md5.hexdigest())

    def test_download_password_absolute_p22(self):
        self._logger.debug('Download with password and absolute path on port 22')
        comparison_hash = '9cce32b2829b5c73b70075cef7210458'
        local_file = '/tmp/file1'
        remote_file = 'sftp://root:password@localhost:/root/testfile'

        downloader = SFTPDownloader(remote_file, local_file)

        downloader.download()

        while not downloader.completed():
            pass

        downloaded_md5 = hashlib.md5()
        with open(local_file, 'rb') as f:
            for chunk in iter(lambda: f.read(8192), b""):
                downloaded_md5.update(chunk)

        self.assertEqual(comparison_hash, downloaded_md5.hexdigest())

    def test_download_pkey_relative_p22(self):
        self._logger.debug('Download with private key and relative path on port 22')
        comparison_hash = '9cce32b2829b5c73b70075cef7210458'
        local_file = '/tmp/file1'
        remote_file = 'sftp://root:password@localhost:testfile'

        downloader = SFTPDownloader(remote_file, local_file, private_key='../dockerfiles/sftp/sshkey')

        downloader.download()

        while not downloader.completed():
            pass

        downloaded_md5 = hashlib.md5()
        with open(local_file, 'rb') as f:
            for chunk in iter(lambda: f.read(8192), b""):
                downloaded_md5.update(chunk)

        self.assertEqual(comparison_hash, downloaded_md5.hexdigest())

    def test_download_pkey_absolute_p22(self):
        self._logger.debug('Download with private key and absolute path on port 22')
        comparison_hash = '9cce32b2829b5c73b70075cef7210458'
        local_file = '/tmp/file1'
        remote_file = 'sftp://root:password@localhost:/root/testfile'

        downloader = SFTPDownloader(remote_file, local_file, private_key='../dockerfiles/sftp/sshkey')

        downloader.download()

        while not downloader.completed():
            pass

        downloaded_md5 = hashlib.md5()
        with open(local_file, 'rb') as f:
            for chunk in iter(lambda: f.read(8192), b""):
                downloaded_md5.update(chunk)

        self.assertEqual(comparison_hash, downloaded_md5.hexdigest())

    def test_download_password_relative_p23(self):
        self._logger.debug('Download with password and relative path on port 22')
        comparison_hash = '9cce32b2829b5c73b70075cef7210458'
        local_file = '/tmp/file1'
        remote_file = 'sftp://root:password@test.local:testfile'

        downloader = SFTPDownloader(remote_file, local_file, port=23)

        downloader.download()

        while not downloader.completed():
            pass

        downloaded_md5 = hashlib.md5()
        with open(local_file, 'rb') as f:
            for chunk in iter(lambda: f.read(8192), b""):
                downloaded_md5.update(chunk)

        self.assertEqual(comparison_hash, downloaded_md5.hexdigest())

    def test_download_password_absolute_p23(self):
        self._logger.debug('Download with password and absolute path on port 22')
        comparison_hash = '9cce32b2829b5c73b70075cef7210458'
        local_file = '/tmp/file1'
        remote_file = 'sftp://root:password@localhost:/root/testfile'

        downloader = SFTPDownloader(remote_file, local_file, port=23)

        downloader.download()

        while not downloader.completed():
            pass

        downloaded_md5 = hashlib.md5()
        with open(local_file, 'rb') as f:
            for chunk in iter(lambda: f.read(8192), b""):
                downloaded_md5.update(chunk)

        self.assertEqual(comparison_hash, downloaded_md5.hexdigest())

    def test_download_pkey_relative_p23(self):
        self._logger.debug('Download with private key and relative path on port 22')
        comparison_hash = '9cce32b2829b5c73b70075cef7210458'
        local_file = '/tmp/file1'
        remote_file = 'sftp://root:password@localhost:testfile'

        downloader = SFTPDownloader(remote_file, local_file, private_key='../dockerfiles/sftp/sshkey',
                                    port=23)

        downloader.download()

        while not downloader.completed():
            pass

        downloaded_md5 = hashlib.md5()
        with open(local_file, 'rb') as f:
            for chunk in iter(lambda: f.read(8192), b""):
                downloaded_md5.update(chunk)

        self.assertEqual(comparison_hash, downloaded_md5.hexdigest())

    def test_download_pkey_absolute_p23(self):
        self._logger.debug('Download with private key and absolute path on port 22')
        comparison_hash = '9cce32b2829b5c73b70075cef7210458'
        local_file = '/tmp/file1'
        remote_file = 'sftp://root:password@localhost:/root/testfile'

        downloader = SFTPDownloader(remote_file, local_file, private_key='../dockerfiles/sftp/sshkey',
                                    port=23)

        downloader.download()

        while not downloader.completed():
            pass

        downloaded_md5 = hashlib.md5()
        with open(local_file, 'rb') as f:
            for chunk in iter(lambda: f.read(8192), b""):
                downloaded_md5.update(chunk)

        self.assertEqual(comparison_hash, downloaded_md5.hexdigest())

    def test_download_pkey_absolute_p23_main_script(self):
        self._logger.debug('Download with private key and absolute path on port 22')
        comparison_hash = '9cce32b2829b5c73b70075cef7210458'
        local_file = '/tmp/file1'
        remote_file = 'sftp://root:password@localhost:/root/testfile'

        downloader = pyDownloader.get_downloader(remote_file, local_file, private_key='../dockerfiles/sftp/sshkey',
                                    port=23)

        downloader.download()

        while not downloader.completed():
            pass

        downloaded_md5 = hashlib.md5()
        with open(local_file, 'rb') as f:
            for chunk in iter(lambda: f.read(8192), b""):
                downloaded_md5.update(chunk)

        self.assertEqual(comparison_hash, downloaded_md5.hexdigest())